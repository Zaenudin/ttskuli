package com.olden.crossword;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPrefs {
    SharedPreferences sp;
    SharedPreferences.Editor spEditor;

    public static final String SP_APP = "SP_APP";
    public static final String SP_LETTER = "LETTER";
    public static final String SP_HINT = "HINT";
    public static final String SP_HINT_TOTAL = "HINT_TOTAL";
    public static final String SP_COLOR_THEME = "SP_COLOR_THEME";


    public SharedPrefs(Context context) {
        sp = context.getSharedPreferences(SP_APP, Context.MODE_PRIVATE);
        spEditor = sp.edit();
    }

    public void saveSPString(String keySP, String value) {
        spEditor.putString(keySP, value);
        spEditor.commit();
    }

    public void saveSPInt(String keySP, int value) {
        spEditor.putInt(keySP, value);
        spEditor.commit();
    }

    public void saveSPFloat(String keySP, float value) {
        spEditor.putFloat(keySP, value);
        spEditor.commit();
    }

    public void saveSPBoolean(String keySP, boolean value) {
        spEditor.putBoolean(keySP, value);
        spEditor.commit();
    }

    public float getSpPercent(int id) {
        return sp.getFloat(SP_LETTER + id, 5);
    }

    public int getSpHint(int id) {
        return sp.getInt(SP_HINT + id, 10);
    }

    public int getSpColor() {
        return sp.getInt(SP_COLOR_THEME, 0);
    }

    public int getSpHintUsed(int id) {
        return sp.getInt(SP_HINT_TOTAL + id, 0);
    }


}
