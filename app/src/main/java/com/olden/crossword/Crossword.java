package com.olden.crossword;

import android.app.ActionBar.LayoutParams;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatDelegate;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnSystemUiVisibilityChangeListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdRequest.Builder;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.reward.RewardItem;
import com.google.android.gms.ads.reward.RewardedVideoAd;
import com.google.android.gms.ads.reward.RewardedVideoAdListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static com.olden.crossword.CustomDialogClass.HINT_DIALOG;
import static com.olden.crossword.CustomDialogClass.NEXT_DIALOG;
import static com.olden.crossword.CustomDialogClass.SHARE_DIALOG;

public class Crossword extends Activity implements OnClickListener, RewardedVideoAdListener, CallbackThis {
    private SharedPreferences sp;
    private SharedPreferences sp_main;
    int screen_width;
    int screen_height;
    int current_word = -1;
    int current_letter;
    int cols;
    int rows;
    float letter;
    boolean cell_click;
    final List<String> cells = new ArrayList<String>();
    final List<List<Integer>> words = new ArrayList<List<Integer>>();
    final List<String> questions = new ArrayList<String>();
    private SharedPrefs sharedPrefs;
    private ImageButton btnErase, btnFriend, btnBack, btnHintPlay, btnHint;
    private LinearLayout btnPlay, btnImg, bgAll, keybord;
    private TextView tvQuestion, tvHint, tvLevel;
    private int hintTemp, hintTot, textColor;
    private int idTemp;

    // AdMob
    private AdView adMobBanner;
    private RewardedVideoAd mRewardedVideoAd;

    private InterstitialAd adMobInterstitial;
    private AdRequest adRequest;
    private int color = 0;
    Button buttons[] = new Button[26];
    private Button bt1, bt2, bt3, bt4, bt5, bt6, bt7, bt8, bt9, bt10;
    private Button bt11, bt12, bt13, bt14, bt15, bt16, bt17, bt18, bt19, bt20;
    private Button bt21, bt22, bt23, bt24, bt25, bt26;
    int adMobInterstitialInterval = 5; // show InterstitialAd after each 5 completed words

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.crossword);
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        initView();


        // fullscreen
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        // preferences
        initPref();
        // hide navigation bar listener
        hideNavigationBar();
        findViewById(R.id.root).setOnSystemUiVisibilityChangeListener(new OnSystemUiVisibilityChangeListener() {
            @Override
            public void onSystemUiVisibilityChange(int visibility) {
                hideNavigationBar();
            }
        });
        setColor();
        textColor = getIntent().getIntExtra("textColor", R.color.button_text);
        for (int i = 0; i < 26; i++) {
            int idx = i + 1;
            String buttonID = "btn_key_" + idx;
            int resID = getResources().getIdentifier(buttonID, "id", getPackageName());
            buttons[i] = ((Button) findViewById(resID));
            buttons[i].setTextColor(textColor);

        }
        tvQuestion.setTextColor(textColor);
        // mess
        ((TextView) findViewById(R.id.mess)).setTextSize(TypedValue.COMPLEX_UNIT_PX, DpToPx(24));

        // AdMob
        adMobInter();
        adMobReward();

        // start
        findViewById(R.id.root).post(new Runnable() {
            @Override
            public void run() {
                START();
            }
        });

    }

    private void initPref() {
        sp = getSharedPreferences(getIntent().getStringExtra("id"), MODE_PRIVATE);
        sp_main = getSharedPreferences("crosswords", MODE_PRIVATE);
        idTemp = Integer.parseInt(getIntent().getStringExtra("id"));
        sharedPrefs = new SharedPrefs(this);
        hintTemp = sharedPrefs.getSpHint(idTemp);
        hintTot = sharedPrefs.getSpHintUsed(idTemp);
        tvHint.setText(String.valueOf(hintTemp));
    }

    private void initView() {
        tvQuestion = findViewById(R.id.tv_question);
        tvLevel = findViewById(R.id.tv_level);
        tvHint = findViewById(R.id.tv_num_hint);
        btnImg = findViewById(R.id.imgBtn);
        btnPlay = findViewById(R.id.btn_play_vid);
        btnErase = findViewById(R.id.btn_erase);
        btnBack = findViewById(R.id.btn_back);
        btnFriend = findViewById(R.id.btn_friend);
        btnHintPlay = findViewById(R.id.btn_hint_play);
        btnHint = findViewById(R.id.btn_hint);
        keybord = findViewById(R.id.keyboard);
        bgAll = findViewById(R.id.all);

        btnImg.setOnClickListener(this);
        btnBack.setOnClickListener(this);
        btnPlay.setOnClickListener(this);
        btnErase.setOnClickListener(this);
        btnFriend.setOnClickListener(this);
        btnHintPlay.setOnClickListener(this);
        btnHint.setOnClickListener(this);


        buttons = new Button[]{bt1, bt2, bt3, bt4, bt5, bt6, bt7, bt8, bt9, bt10,
                bt11, bt12, bt13, bt14, bt15, bt16, bt17, bt18, bt19, bt20,
                bt21, bt22, bt23, bt24, bt25, bt26};
        tvQuestion.setLines(1);
        tvQuestion.setHorizontallyScrolling(true);
        tvQuestion.setMarqueeRepeatLimit(-1);
        tvQuestion.setSelected(true);
    }

    private void setColor() {
        switch (sharedPrefs.getSpColor()) {
            case 1:
                color = getResources().getColor(R.color.tema1);
                break;
            case 2:
                color = getResources().getColor(R.color.tema2);
                break;
            case 3:
                color = getResources().getColor(R.color.tema3);
                break;
            case 4:
                color = getResources().getColor(R.color.tema4);
                break;
            case 5:
                color = getResources().getColor(R.color.tema5);
                break;
            case 6:
                color = getResources().getColor(R.color.tema6);
                break;
            case 7:
                color = getResources().getColor(R.color.tema7);
                break;
            default:
                color = getResources().getColor(R.color.tema1);
                break;
        }

        bgAll.setBackgroundColor(color);
        btnErase.setBackgroundColor(color);
        btnPlay.setBackgroundColor(color);
        btnFriend.setBackgroundColor(color);
        btnHintPlay.setBackgroundColor(color);
        btnHint.setBackgroundColor(color);
        btnImg.setBackgroundColor(color);
        keybord.setBackgroundColor(color);


    }

    private void adMobReward() {
        MobileAds.initialize(this, "ca-app-pub-3940256099942544~3347511713");

        // Use an activity context to get the rewarded video instance.
        mRewardedVideoAd = MobileAds.getRewardedVideoAdInstance(this);
        mRewardedVideoAd.setRewardedVideoAdListener(this);
        loadRewardedVideoAd();
    }

    // READ_FILE
    void READ_FILE(String path) throws IOException, JSONException {
        StringBuilder data = new StringBuilder();
        InputStream stream = getAssets().open(path);
        BufferedReader in = new BufferedReader(new InputStreamReader(stream));
        String line;
        while ((line = in.readLine()) != null)
            data.append(line);
        in.close();

        // json
        JSONObject json = new JSONObject(data.toString());

        // cols and rows
        cols = json.getInt("cols");
        rows = json.getInt("rows");
        JSONArray cellArray = json.getJSONArray("cells");
        letter = cellArray.length();

        for (int i = 0; i < cellArray.length(); i++) {
            if (cellArray.getString(i).equals("") || cellArray.getString(i).isEmpty()) {
                letter--;
            }
        }
//        Log.e("letter "+getIntent().getStringExtra("id"), String.valueOf((letter/letter)*100));
        setPercent();
        // cells
        for (int i = 0; i < json.getJSONArray("cells").length(); i++)
            cells.add(json.getJSONArray("cells").getString(i));

        // questions
        for (int i = 0; i < json.getJSONArray("questions").length(); i++)
            questions.add(json.getJSONArray("questions").getString(i));

        // words
        for (int i = 0; i < json.getJSONArray("words").length(); i++) {
            String[] temp_array = json.getJSONArray("words").getString(i).split(",");
            List<Integer> word_array = new ArrayList<Integer>();
            for (int j = 0; j < temp_array.length; j++)
                word_array.add(Integer.parseInt(temp_array[j]));

            words.add(word_array);
        }

        // save all words number
        sp_main.edit().putInt("words_all" + getIntent().getStringExtra("id"), words.size()).commit();
    }

    // START
    void START() {
        // keyboard buttons text size
        Button btn;
        for (int j = 0; j < ((ViewGroup) findViewById(R.id.keyboard)).getChildCount(); j++)
            for (int i = 0; i < ((ViewGroup) ((ViewGroup) findViewById(R.id.keyboard)).getChildAt(j)).getChildCount(); i++) {
                btn = ((Button) ((ViewGroup) ((ViewGroup) findViewById(R.id.keyboard)).getChildAt(j)).getChildAt(i));
                btn.setTextSize(TypedValue.COMPLEX_UNIT_PX, (int) Math.min(btn.getWidth(), btn.getHeight()) * 0.6f);
            }

        // read file
        try {
            READ_FILE(getIntent().getStringExtra("id") + ".txt");

            // show mess if completed
            if (sp_main.contains("words_completed" + getIntent().getStringExtra("id"))
                    && sp_main.getInt("words_completed" + getIntent().getStringExtra("id"), 0) == sp_main.getInt("words_all"
                    + getIntent().getStringExtra("id"), 0)) {
//                findViewById(R.id.mess).setVisibility(View.VISIBLE);
                showDialogCustom(1);
//                showInterstitial();
                setPercent();
            }
        } catch (IOException e) {
            Log.e("errIO", e.getMessage());
            ((TextView) findViewById(R.id.mess)).setText(getString(R.string.error));
            findViewById(R.id.all).setVisibility(View.GONE);
            findViewById(R.id.mess).setVisibility(View.VISIBLE);
            return;
        } catch (JSONException e) {
            Log.e("errJSON", e.getMessage());
            ((TextView) findViewById(R.id.mess)).setText(getString(R.string.error));
            findViewById(R.id.all).setVisibility(View.GONE);
            findViewById(R.id.mess).setVisibility(View.VISIBLE);
            return;
        }

        // show all
        findViewById(R.id.all).setVisibility(View.VISIBLE);

        // screen size
        screen_width = findViewById(R.id.game).getWidth();
        screen_height = findViewById(R.id.game).getHeight();

        // cell size
        int cell_size = Math.min(screen_width / cols, screen_height / rows);

        // frame
        findViewById(R.id.frame).getLayoutParams().width = cell_size * cols;
        findViewById(R.id.frame).getLayoutParams().height = cell_size * rows;

        // tags
        String[] tags = new String[cols * rows];
        for (int i = 0; i < words.size(); i++)
            for (int j = 0; j < words.get(i).size(); j++)
                if (tags[words.get(i).get(j)] == null)
                    tags[words.get(i).get(j)] = String.valueOf(i);
                else
                    tags[words.get(i).get(j)] = tags[words.get(i).get(j)] + "," + String.valueOf(i);

        // add questions
        boolean row1 = false;
        for (int i = 0; i < questions.size(); i++) {
            TextView item = new TextView(this);
            item.setTextSize(TypedValue.COMPLEX_UNIT_PX, DpToPx(14));
            item.setPadding((int) DpToPx(3), (int) DpToPx(3), (int) DpToPx(3), (int) DpToPx(3));
            item.setLayoutParams(new LayoutParams(android.view.ViewGroup.LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
            item.setBackgroundResource(row1 ? R.drawable.row1 : R.drawable.row2);
            row1 = !row1;
            item.setTag(i);
            item.setText((i + 1) + ". " + questions.get(i));
            item.setTypeface(Typeface.DEFAULT);
            item.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    // show_word
                    if (current_word != Integer.parseInt(v.getTag().toString())) {
                        cell_click = false;
                        current_letter = words.get(Integer.parseInt(v.getTag().toString())).get(0);
                        showWord(Integer.parseInt(v.getTag().toString()));
                    }
                }
            });
            ((ViewGroup) findViewById(R.id.list)).addView(item);
        }

        // add cells
        int x_pos = 0;
        int y_pos = 0;
        for (int i = 0; i < rows * cols; i++) {
            TextView cell = new TextView(this);
            cell.setLayoutParams(new LayoutParams(cell_size, cell_size));

            // enabled cell
            if (!cells.get(i).equals("")) {
                cell.setText(sp.getString("cell_letter" + i, ""));
                cell.setBackgroundResource(R.drawable.cell_default);
                cell.setTag(tags[i] + "," + i);
                cell.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        cell_click = true;
                        String[] temp_array = v.getTag().toString().split(",");
                        current_letter = Integer.parseInt(temp_array[temp_array.length - 1]);

                        // show_word
                        if (temp_array.length > 2)
                            if (current_word == Integer.valueOf(temp_array[0]))
                                showWord(Integer.valueOf(temp_array[1]));
                            else
                                showWord(Integer.valueOf(temp_array[0]));
                        else
                            showWord(Integer.valueOf(temp_array[0]));
                    }
                });
            }

            // letter done
            if (sp.getBoolean("cell_disabled" + i, false))
                cell.setBackgroundResource(R.drawable.cell_disabled);

            cell.setTextSize(TypedValue.COMPLEX_UNIT_PX, (int) cell_size * 0.65f);
            cell.setX(x_pos * cell_size);
            cell.setY(y_pos * cell_size);
            ((ViewGroup) findViewById(R.id.frame)).addView(cell);

            x_pos++;
            if (x_pos == cols) {
                x_pos = 0;
                y_pos++;
            }
        }
//        showWord(0);
        tvLevel.setText("level " + idTemp);
        tvQuestion.setText("Welcome to " + getString(R.string.app_name));

    }

    private void showDialogCustom(int i) {
        switch (i) {
            case 1:
                CustomDialogClass cdd = new CustomDialogClass(this, String.valueOf((((letter - hintTot) / letter) * 100)), String.valueOf(idTemp), color, this);
                cdd.show();
                break;
            case 2:
                CustomDialogClass cdd2 = new CustomDialogClass(this, HINT_DIALOG, HINT_DIALOG, color, this);
                cdd2.show();
                break;
        }

    }

    // showWord
    void showWord(int num) {
        // clear current selection
        if (current_word != -1)
            for (int i = 0; i < words.get(current_word).size(); i++) {
                ((TextView) ((ViewGroup) findViewById(R.id.frame)).getChildAt(words.get(current_word).get(i)))
                        .setTextColor(getResources().getColor(R.color.text_color));

                if (!sp.getBoolean("cell_disabled" + words.get(current_word).get(i), false))
                    ((TextView) ((ViewGroup) findViewById(R.id.frame)).getChildAt(words.get(current_word).get(i)))
                            .setBackgroundResource(R.drawable.cell_default);
                else
                    ((TextView) ((ViewGroup) findViewById(R.id.frame)).getChildAt(words.get(current_word).get(i)))
                            .setBackgroundResource(R.drawable.cell_disabled);
            }

        current_word = num;

        // show new selection
        for (int i = 0; i < words.get(current_word).size(); i++)
            ((TextView) ((ViewGroup) findViewById(R.id.frame)).getChildAt(words.get(current_word).get(i)))
                    .setBackgroundResource(R.drawable.cell_selected);

        // current input letter
        if (!sp.getBoolean("cell_disabled" + current_letter, false)) {
            ((TextView) ((ViewGroup) findViewById(R.id.frame)).getChildAt(current_letter)).setTextColor(getResources().getColor(
                    R.color.button_text));
            ((TextView) ((ViewGroup) findViewById(R.id.frame)).getChildAt(current_letter))
                    .setBackgroundResource(R.drawable.cell_current);
        }

        // hide selection in questions
        boolean row1 = false;
        for (int i = 0; i < ((ViewGroup) findViewById(R.id.list)).getChildCount(); i++) {
            ((ViewGroup) findViewById(R.id.list)).getChildAt(i).setBackgroundResource(row1 ? R.drawable.row1 : R.drawable.row2);
            row1 = !row1;
        }

        // show selection in questions
        ((ViewGroup) findViewById(R.id.list)).getChildAt(current_word).setBackgroundColor(
                getResources().getColor(R.color.select_bg));

        // scroll list
        if (cell_click)
            tvQuestion.setText(questions.get(current_word));

    }


    // onKey
    public void onKey(View v) {
        if (current_word != -1 && !sp.getBoolean("cell_disabled" + current_letter, false)) {
            // save letter
            if (v != null) {
                sp.edit().putString("cell_letter" + current_letter, ((Button) v).getText().toString()).commit();
                ((TextView) ((ViewGroup) findViewById(R.id.frame)).getChildAt(current_letter)).setText(((Button) v).getText()
                        .toString());
            }
            // check current words complete
            keyEvent();
        }
    }

    private void keyEvent() {
        for (int j = 0; j < words.get(current_word).size(); j++)
            if (!((TextView) ((ViewGroup) findViewById(R.id.frame)).getChildAt(words.get(current_word).get(j))).getText()
                    .toString().equalsIgnoreCase(cells.get(words.get(current_word).get(j)))) {

                // next letter position
                for (int i = words.get(current_word).indexOf(current_letter) + 1; i < words.get(current_word).size(); i++) {
                    if (!sp.getBoolean("cell_disabled" + words.get(current_word).get(i), false)) {
                        ((TextView) ((ViewGroup) findViewById(R.id.frame)).getChildAt(current_letter))
                                .setBackgroundResource(R.drawable.cell_selected);
                        ((TextView) ((ViewGroup) findViewById(R.id.frame)).getChildAt(current_letter))
                                .setTextColor(getResources().getColor(R.color.text_color));
                        current_letter = words.get(current_word).get(i);
                        ((TextView) ((ViewGroup) findViewById(R.id.frame)).getChildAt(current_letter))
                                .setBackgroundResource(R.drawable.cell_current);
                        ((TextView) ((ViewGroup) findViewById(R.id.frame)).getChildAt(current_letter))
                                .setTextColor(getResources().getColor(R.color.button_text));
                        break;
                    }
                }

                return;
            }

        // hide selection in questions
        boolean row1 = false;
        for (int i = 0; i < ((ViewGroup) findViewById(R.id.list)).getChildCount(); i++) {
            ((ViewGroup) findViewById(R.id.list)).getChildAt(i).setBackgroundResource(
                    row1 ? R.drawable.row1 : R.drawable.row2);
            row1 = !row1;
        }

        // word complete
        sp_main.edit()
                .putInt("words_completed" + getIntent().getStringExtra("id"),
                        sp_main.getInt("words_completed" + getIntent().getStringExtra("id"), 0) + 1).commit();
        sp.edit().putBoolean("word_completed" + current_word, true).commit();

        // hide selection
        for (int i = 0; i < words.get(current_word).size(); i++) {
            ((TextView) ((ViewGroup) findViewById(R.id.frame)).getChildAt(words.get(current_word).get(i)))
                    .setTextColor(getResources().getColor(R.color.text_color));
            ((TextView) ((ViewGroup) findViewById(R.id.frame)).getChildAt(words.get(current_word).get(i)))
                    .setBackgroundResource(R.drawable.cell_disabled);
            sp.edit().putBoolean("cell_disabled" + words.get(current_word).get(i), true).commit();
        }

        // check all words completed
        for (int i = 0; i < words.size(); i++) {
            boolean complete = true;

            if (!sp.getBoolean("word_completed" + i, false)) {
                // check all letters of the word
                for (int j = 0; j < words.get(i).size(); j++) {
                    if (!((TextView) ((ViewGroup) findViewById(R.id.frame)).getChildAt(words.get(i).get(j))).getText()
                            .toString().equalsIgnoreCase(cells.get(words.get(i).get(j)))) {
                        complete = false;
                        break;
                    }
                }

                // word complete
                if (complete) {
                    // save
                    sp_main.edit()
                            .putInt("words_completed" + getIntent().getStringExtra("id"),
                                    sp_main.getInt("words_completed" + getIntent().getStringExtra("id"), 0) + 1).commit();
                    sp.edit().putBoolean("word_completed" + i, true).commit();

                    // disable cells
                    for (int j = 0; j < words.get(i).size(); j++) {
                        ((TextView) ((ViewGroup) findViewById(R.id.frame)).getChildAt(words.get(i).get(j)))
                                .setTextColor(getResources().getColor(R.color.text_color));
                        ((TextView) ((ViewGroup) findViewById(R.id.frame)).getChildAt(words.get(i).get(j)))
                                .setBackgroundResource(R.drawable.cell_disabled);
                        sp.edit().putBoolean("cell_disabled" + words.get(i).get(j), true).commit();
                    }
                }
            }
        }

        // if all complete
        if (sp_main.contains("words_completed" + getIntent().getStringExtra("id"))
                && sp_main.getInt("words_completed" + getIntent().getStringExtra("id"), 0) == sp_main.getInt("words_all"
                + getIntent().getStringExtra("id"), 0)) {

            // mess
//            findViewById(R.id.mess).setVisibility(View.VISIBLE);
            setPercent();


            // AdMob Interstitial
            showInterstitial();

        }
    }

    private void showInterstitial() {
        sp.edit().putInt("admob", !sp.contains("admob") ? 1 : sp.getInt("admob", 0) + 1).commit();
        if (adMobInterstitial.isLoaded()) {
//                    if (sp.getInt("admob", 0) >= adMobInterstitialInterval) {
//                        sp.edit().putInt("admob", 0).commit();
            adMobInterstitial.show(); // show
            adMobInterstitial.setAdListener(new AdListener(){
                @Override
                public void onAdClosed() {
                    super.onAdClosed();
                    showDialogCustom(1);
                }
            });
        } else if (!adMobInterstitial.isLoading() && ((ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE)).getActiveNetworkInfo() != null)
            adMobInterstitial.loadAd(adRequest); // load
    }

    // DpToPx
    float DpToPx(float dp) {
        return (dp * Math.max(getResources().getDisplayMetrics().widthPixels, getResources().getDisplayMetrics().heightPixels) / 540f);
    }

    // hide_navigation_bar
    void hideNavigationBar() {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus)
            hideNavigationBar();
    }

    @Override
    protected void onDestroy() {
        // destroy AdMob
        if (adMobBanner != null) {
            adMobBanner.setAdListener(null);
            adMobBanner.destroyDrawingCache();
            adMobBanner.destroy();
            adMobBanner = null;
        }
        if (adMobInterstitial != null) {
            adMobInterstitial.setAdListener(null);
            adMobInterstitial = null;
        }
        adRequest = null;

        super.onDestroy();
    }


    private void setPercent() {
        sharedPrefs.saveSPFloat(SharedPrefs.SP_LETTER + idTemp, (((letter - hintTot) / letter) * 100));
    }


    // adMob

    private void adMobInter() {
        if (getResources().getBoolean(R.bool.show_admob)) {
            // make AdMob request
            Builder builder = new Builder();
//            if (getResources().getBoolean(R.bool.admob_test))
//                builder.addTestDevice(AdRequest.DEVICE_ID_EMULATOR).addTestDevice(
//                        MD5(Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID)));
            adRequest = builder.build();

            // AdMob Interstitial
            adMobInterstitial = new InterstitialAd(this);
            adMobInterstitial.setAdUnitId(getString(R.string.adMob_interstitial));
            adMobInterstitial.setAdListener(new AdListener() {
                public void onAdClosed() {
                    if (((ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE)).getActiveNetworkInfo() != null)
                        adMobInterstitial.loadAd(adRequest);
                }
            });

            if (((ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE)).getActiveNetworkInfo() != null) {
                // AdMob Banner
                adMobBanner = new AdView(this);
                adMobBanner.setAdUnitId(getString(R.string.adMob_banner));
                adMobBanner.setAdSize(AdSize.SMART_BANNER);
                ((ViewGroup) findViewById(R.id.admob)).addView(adMobBanner);

                // load
                adMobBanner.loadAd(adRequest);
                adMobInterstitial.loadAd(adRequest);
            }
        }
    }


    // MD5
    String MD5(String str) {
        try {
            java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
            byte[] array = md.digest(str.getBytes());
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < array.length; ++i)
                sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1, 3));
            return sb.toString().toUpperCase(Locale.ENGLISH);
        } catch (java.security.NoSuchAlgorithmException e) {
        }
        return null;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.mess:
                if (findViewById(R.id.all).getVisibility() == View.VISIBLE)
                    view.setVisibility(View.GONE);
                else
                    onBackPressed();
                break;

            case R.id.imgBtn: {
                //    cell.setText(sp.getString("cell_letter" + i, ""));
                hintWord();
            }
            break;
            case R.id.btn_erase: {
                if (current_word != -1 && !sp.getBoolean("cell_disabled" + current_letter, false)) {
                    // save letter
                    sp.edit().putString("cell_letter" + current_letter, "").commit();
                    ((TextView) ((ViewGroup) findViewById(R.id.frame)).getChildAt(current_letter)).setText("");

                    // check current words complete
                    keyEvent();
                }

            }
            break;
            case R.id.btn_play_vid: {
                //   loadRewardedVideoAd();
//                playVid();
                showDialogCustom(2);
            }
            break;

            case R.id.btn_friend: {
                //   loadRewardedVideoAd();
                Toast.makeText(this, "friend", Toast.LENGTH_SHORT).show();
            }
            break;
            case R.id.btn_back: {
                //   loadRewardedVideoAd();
                onBackPressed();
            }
            break;

            case R.id.btn_hint:
                hintWord();
                break;
            case R.id.btn_hint_play:
//                playVid();
                showDialogCustom(2);
                break;
        }
    }

    private void playVid() {
        if (mRewardedVideoAd.isLoaded()) {
            mRewardedVideoAd.show();
        }
    }

    private void hintWord() {
        String lett = sp.getString("cell_letter" + current_letter, "");
        String gett = cells.get(current_letter);
        if (lett.equals(gett)) {
            Toast.makeText(this, "Huruf sudah tepat", Toast.LENGTH_SHORT).show();
        } else {
            if (hintTemp < 1) {
                Toast.makeText(this, "Bantuan sudah habis tot", Toast.LENGTH_SHORT).show();
            } else {
//                hintTemp--;
                hintTot++;
                tvHint.setText(String.valueOf(hintTemp));
                sharedPrefs.saveSPInt(SharedPrefs.SP_HINT + idTemp, hintTemp);
                sharedPrefs.saveSPInt(SharedPrefs.SP_HINT_TOTAL + idTemp, hintTot);
                sp.edit().putString("cell_letter" + current_letter, cells.get(current_letter)).commit();
                ((TextView) ((ViewGroup) findViewById(R.id.frame)).getChildAt(current_letter)).setText(cells.get(current_letter));
                onKey(null);
                setPercent();
            }
        }
    }

    private void loadRewardedVideoAd() {
        mRewardedVideoAd.loadAd("ca-app-pub-3940256099942544/5224354917",
                new AdRequest.Builder().build());
    }

    @Override
    public void onRewardedVideoAdLoaded() {

    }

    @Override
    public void onRewardedVideoAdOpened() {

    }

    @Override
    public void onRewardedVideoStarted() {

    }

    @Override
    public void onRewardedVideoAdClosed() {
        loadRewardedVideoAd();
    }

    @Override
    public void onRewarded(RewardItem rewardItem) {
        hintTemp = 10;
        tvHint.setText(String.valueOf(hintTemp));
        sharedPrefs.saveSPInt(SharedPrefs.SP_HINT + idTemp, hintTemp);
    }

    @Override
    public void onRewardedVideoAdLeftApplication() {

    }

    @Override
    public void onRewardedVideoAdFailedToLoad(int i) {
        loadRewardedVideoAd();
    }

    @Override
    public void onRewardedVideoCompleted() {
        loadRewardedVideoAd();
    }

    @Override
    public void whatCallback(String method) {
        if (method.equals(HINT_DIALOG))
            playVid();
        else if (method.equals(SHARE_DIALOG))
            shareCompleted();
        else if (method.equals(NEXT_DIALOG))
            finish();
        else
            Toast.makeText(this, "OTher", Toast.LENGTH_SHORT).show();
    }

    private void shareCompleted() {
        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, getResources().getString(R.string.app_name));
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Aku telah menyelesaikan TTS level " + idTemp + " : market://details?id=" + getPackageName());
        startActivity(Intent.createChooser(sharingIntent, "Share using"));
    }


}

