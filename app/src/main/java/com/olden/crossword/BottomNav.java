package com.olden.crossword;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import static com.olden.crossword.SharedPrefs.SP_COLOR_THEME;

public class BottomNav extends AppCompatActivity implements View.OnClickListener {

    Button play_game, open_materi, warna1, warna2, warna3, warna4, warna5, warna6, warna7;
    private TextView mTextMessage;
    private BottomNavigationView navigation;
    private SharedPreferences sp;
    private SharedPrefs sharedPrefs;
    private int color = 0;
    private int textColor = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.bottom_nav);
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        initView();


        warna1.setOnClickListener(this);
        warna2.setOnClickListener(this);
        warna3.setOnClickListener(this);
        warna4.setOnClickListener(this);
        warna5.setOnClickListener(this);
        warna6.setOnClickListener(this);
        warna7.setOnClickListener(this);
        play_game.setOnClickListener(this);

        setColor();

    }

    private void initView() {
        sp = getSharedPreferences("crosswords", MODE_PRIVATE);
        mTextMessage = (TextView) findViewById(R.id.message);
        navigation = findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        sharedPrefs = new SharedPrefs(this);
        play_game = (Button) findViewById(R.id.play);
        open_materi = (Button) findViewById(R.id.materi);

        open_materi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               // Intent intent = new Intent(BottomNav.this,Materials.class);
                //startActivity(intent);
            }
        });


        warna1 = (Button) findViewById(R.id.tema1);
        warna2 = (Button) findViewById(R.id.tema2);
        warna3 = (Button) findViewById(R.id.tema3);
        warna4 = (Button) findViewById(R.id.tema4);
        warna5 = (Button) findViewById(R.id.tema5);
        warna6 = (Button) findViewById(R.id.tema6);
        warna7 = (Button) findViewById(R.id.tema7);


    }

    private void setColor() {
        textColor = getResources().getColor(R.color.text_color);
        switch (sharedPrefs.getSpColor()) {
            case 1:
                color = getResources().getColor(R.color.tema1);
                textColor = getResources().getColor(R.color.button_text);
                break;
            case 2:
                color = getResources().getColor(R.color.tema2);
                textColor = getResources().getColor(R.color.button_text);
                break;
            case 3:
                color = getResources().getColor(R.color.tema3);
                break;
            case 4:
                color = getResources().getColor(R.color.tema4);
                break;
            case 5:
                color = getResources().getColor(R.color.tema5);
                textColor = getResources().getColor(R.color.button_text);

                break;
            case 6:
                color = getResources().getColor(R.color.tema6);
                textColor = getResources().getColor(R.color.button_text);

                break;
            case 7:
                color = getResources().getColor(R.color.tema7);
                break;
            default:
                color = getResources().getColor(R.color.tema1);
                textColor = getResources().getColor(R.color.button_text);
                break;
        }


        navigation.setBackgroundColor(color);
        play_game.setBackgroundColor(color);
        open_materi.setBackgroundColor(color);
        play_game.setTextColor(textColor);
        open_materi.setTextColor(textColor);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.navigation, menu);
        return true;
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    shareMe();
                    return true;
                case R.id.navigation_dashboard:
                    rateMe();
                    return true;
                case R.id.navigation_notifications:
                    AlertDialog.Builder builder = new AlertDialog.Builder(BottomNav.this);
                    builder.setMessage("TTS Teka Teki Silang 2018 ??????????").setNegativeButton("OK", null).show();
                    //Toast.makeText(BottomNav.this, "notif", Toast.LENGTH_SHORT).show();
                    return true;
            }
            return false;
        }
    };

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tema1: {
                sharedPrefs.saveSPInt(SP_COLOR_THEME, 1);
                setColor();
            }
            break;
            case R.id.tema2: {
                sharedPrefs.saveSPInt(SP_COLOR_THEME, 2);
                setColor();
            }
            break;
            case R.id.tema3: {
                sharedPrefs.saveSPInt(SP_COLOR_THEME, 3);
                setColor();
            }
            break;
            case R.id.tema4: {
                sharedPrefs.saveSPInt(SP_COLOR_THEME, 4);
                setColor();
            }
            break;
            case R.id.tema5: {
                sharedPrefs.saveSPInt(SP_COLOR_THEME, 5);
                setColor();
            }
            break;
            case R.id.tema6: {
                sharedPrefs.saveSPInt(SP_COLOR_THEME, 6);
                setColor();
            }
            break;
            case R.id.tema7: {
                sharedPrefs.saveSPInt(SP_COLOR_THEME, 7);
                setColor();
            }
            break;
            case R.id.materi:
            {
                //startActivity(new Intent(this, Materils.class));
            }
            break;
            case R.id.play: {
                startActivity(new Intent(this, Main.class));
            }


        }

    }

    // rateDialog
    void rateDialog() {
        AlertDialog.Builder adb = new AlertDialog.Builder(this);
        adb.setMessage(R.string.rate);
        adb.setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                rateMe();
            }
        });
        adb.setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        AlertDialog dialog = adb.create();
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        ((TextView) dialog.findViewById(android.R.id.message)).setGravity(Gravity.CENTER);
    }

    private void rateMe() {
        sp.edit().putBoolean("rate", true).commit();
        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + getPackageName())));
        finish();
    }

    @Override
    public void onBackPressed() {
        if (!sp.contains("rate"))
            rateDialog();
        else
            super.onBackPressed();
    }

    private void shareMe() {
        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, getResources().getString(R.string.app_name));
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "lets play with me : market://details?id=" + getPackageName());
        startActivity(Intent.createChooser(sharingIntent, "Share using"));
    }
}


