package com.olden.crossword;

import android.app.ActionBar.LayoutParams;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatDelegate;
import android.util.TypedValue;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnSystemUiVisibilityChangeListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdRequest.Builder;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;

import java.text.DecimalFormat;

import static com.olden.crossword.CustomDialogClass.LOCK_DIALOG;

public class Main extends Activity implements CallbackThis {
    SharedPreferences sp;
    ViewPager pager;
    SwipeAdapter adapter;
    final int num_pages = 15; // num pages
    final int num_cols = 4; // num cols in page
    final int num_rows = 5; // num rows in page

    private SharedPrefs sharedPrefs;
    private AdView mAdView;
    private int color = 0;
    private int textColor = 0;
    private View footer;
    private int bekgron = 0;

    private InterstitialAd adMobInterstitial;
    private AdRequest adRequest;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        // fullscreen
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        // preferences
        sp = getSharedPreferences("crosswords", MODE_PRIVATE);
        sharedPrefs = new SharedPrefs(this);
        footer = findViewById(R.id.footer_bott);

        // hide navigation bar listener
        hideNavigationBar();
        findViewById(R.id.root).setOnSystemUiVisibilityChangeListener(new OnSystemUiVisibilityChangeListener() {
            @Override
            public void onSystemUiVisibilityChange(int visibility) {
                hideNavigationBar();
            }
        });
        setBanner();
        setColor();

        // pager
        findViewById(R.id.root).post(new Runnable() {
            @Override
            public void run() {
                adapter = new SwipeAdapter();

                // pager
                pager = new VerticalViewPager(Main.this);
                pager.setAdapter(adapter);

                pager.setOffscreenPageLimit(1);
                pager.setCurrentItem(sp.getInt("page", 0));
                ((ViewGroup) findViewById(R.id.root)).addView(pager);
                pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                    @Override
                    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                    }

                    @Override
                    public void onPageScrollStateChanged(int state) {
                    }

                    @Override
                    public void onPageSelected(int position) {
                        sp.edit().putInt("page", position).commit();
                    }
                });
            }
        });
        footer.setBackgroundColor(color);
        adMobInter();
    }

    private void setColor() {
        switch (sharedPrefs.getSpColor()) {
            case 1:
                color = getResources().getColor(R.color.tema1);
                bekgron = R.drawable.row1;
                textColor = getResources().getColor(R.color.button_text);
                break;
            case 2:
                color = getResources().getColor(R.color.tema2);
                bekgron = R.drawable.row2;
                textColor = getResources().getColor(R.color.button_text);
                break;
            case 3:
                color = getResources().getColor(R.color.tema3);
                bekgron = R.drawable.row3;
                textColor = getResources().getColor(R.color.text_color);
                break;
            case 4:
                color = getResources().getColor(R.color.tema4);
                bekgron = R.drawable.row4;
                textColor = getResources().getColor(R.color.text_color);
                break;
            case 5:
                color = getResources().getColor(R.color.tema5);
                bekgron = R.drawable.row5;
                textColor = getResources().getColor(R.color.button_text);

                break;
            case 6:
                color = getResources().getColor(R.color.tema6);
                bekgron = R.drawable.row6;
                textColor = getResources().getColor(R.color.button_text);

                break;
            case 7:
                color = getResources().getColor(R.color.tema7);
                bekgron = R.drawable.row7;
                textColor = getResources().getColor(R.color.text_color);
                break;
            default:
                color = getResources().getColor(R.color.tema1);
                bekgron = R.drawable.row1;
                textColor = getResources().getColor(R.color.button_text);
                break;
        }
    }

    private void setBanner() {
        MobileAds.initialize(this,
                "ca-app-pub-3940256099942544~3347511713");
        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
    }

    @Override
    protected void onResume() {
        super.onResume();

        // update pager
        if (pager != null) {
            pager.setAdapter(adapter);
            pager.setCurrentItem(sp.getInt("page", 0));
        }
    }


    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus)
            hideNavigationBar();
    }


    // DpToPx
    float DpToPx(float dp) {
        return (dp * Math.max(getResources().getDisplayMetrics().widthPixels, getResources().getDisplayMetrics().heightPixels) / 540f);
    }

    // hide_navigation_bar
    void hideNavigationBar() {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
    }

    @Override
    public void whatCallback(String method) {

    }

    // SwipeAdapter
    public class SwipeAdapter extends PagerAdapter {
        @NonNull
        @Override
        public Object instantiateItem(@NonNull ViewGroup container, int position) {
            FrameLayout page = new FrameLayout(getApplicationContext());

            // item_size
            int item_width = (int) Math.ceil((float) findViewById(R.id.root).getWidth() / (float) num_cols);
            int item_height = (int) Math.ceil((float) findViewById(R.id.root).getHeight() / (float) num_rows);

            // add crosswords
            boolean row1 = false;
            int x_pos = 0;
            int y_pos = 0;
            for (int i = position * num_cols * num_rows + 1;
                 i <= position * num_cols * num_rows + num_cols * num_rows;
                 i++) {
                View item = getLayoutInflater().inflate(R.layout.item, page, false);

                row1 = !row1;
                item.setTag(i);
                item.setX(x_pos * item_width);
                item.setY(y_pos * item_height);
                item.setLayoutParams(new LayoutParams(item_width, item_height));

                ((TextView) item.findViewById(R.id.txt_title)).setText(String.valueOf(i));
                ((TextView) item.findViewById(R.id.txt_title)).setTextColor(textColor);
                ((TextView) item.findViewById(R.id.txt_title)).setTextSize(TypedValue.COMPLEX_UNIT_PX, DpToPx(24));
                ((TextView) item.findViewById(R.id.txt_completed)).setTextSize(TypedValue.COMPLEX_UNIT_PX, DpToPx(14));
                ((TextView) item.findViewById(R.id.txt_completed)).setTextColor(textColor);

//                 if last

//                else
//                    item.setBackgroundResource(row1 ? R.drawable.row1 : R.drawable.row2);
                item.setBackgroundResource(bekgron);
//                item.findViewById(R.id.txt_title).setVisibility(View.GONE);
                // if started
                if (sp.contains("words_completed" + i)) {
                    item.findViewById(R.id.img_lock).setVisibility(View.GONE);
                    if (sp.getInt("words_completed" + i, 0) == sp.getInt("words_all" + i, 0)) {
                        // if completed all
                        DecimalFormat df = new DecimalFormat();
                        df.setMaximumFractionDigits(0);
                        float percent = sharedPrefs.getSpPercent(i);
                        ((TextView) item.findViewById(R.id.txt_completed)).setText("Score: " + df.format(percent));
                        item.findViewById(R.id.txt_title).setVisibility(View.VISIBLE);

                    } else {
                        // if completed one or more words
                        item.findViewById(R.id.txt_completed).setVisibility(View.VISIBLE);
                        item.findViewById(R.id.txt_title).setVisibility(View.VISIBLE);
                        ((TextView) item.findViewById(R.id.txt_completed)).setText(sp.getInt("words_completed" + i, 0) + " / "
                                + sp.getInt("words_all" + i, 0));
                    }

                } else {
                    // not started
                    item.findViewById(R.id.txt_completed).setVisibility(View.INVISIBLE);
                    item.findViewById(R.id.txt_title).setVisibility(View.INVISIBLE);
                }
                if (sp.contains("last") && sp.getInt("last", 0) == i ||
                        i == 1
                ) {
                    item.findViewById(R.id.img_lock).setVisibility(View.GONE);
                    item.findViewById(R.id.txt_title).setVisibility(View.VISIBLE);
                }
                if (sp.contains("words_completed" + (i - 1)) || i == 1) {
                    if (sp.getInt("words_completed" + (i - 1), 0) == sp.getInt("words_all" + (i - 1), 0)) {

                        item.findViewById(R.id.img_lock).setVisibility(View.GONE);
                        item.findViewById(R.id.txt_title).setVisibility(View.VISIBLE);
                    }
                }

                // click listener
                clickItem(item, i);

                page.addView(item);

                // next line
                x_pos++;
                if (x_pos == num_cols) {
                    row1 = !row1;
                    x_pos = 0;
                    y_pos++;
                }
            }

            container.addView(page);
            return page;
        }

        private void clickItem(View item, final int level) {

            item.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(final View v) {

                    openCrossword(v, true);

                }

                private void openCrossword(final View v, boolean isLock) {
                    if (isLock) {
                        if (sp.contains("words_completed" + (level - 1)) || level == 1) {
                            if (sp.getInt("words_completed" + (level - 1), 0) == sp.getInt("words_all" + (level - 1), 0)) {

                                openInterstitial(v);
//                            startActivity(new Intent(Main.this, Crossword.class).putExtra("id", v.getTag().toString()).putExtra("textColor", textColor));
                            } else {
                                showLock();
                            }
                        } else {
                            showLock();
                        }
                    } else {
                        openInterstitial(v);
                    }
                }

                private void openInterstitial(final View v) {
                    sp.edit().putInt("last", (Integer) v.getTag()).commit();
                    showInterstitial();
                    adMobInterstitial.setAdListener(new AdListener() {
                        @Override
                        public void onAdClosed() {
                            super.onAdClosed();
                            startActivity(new Intent(Main.this, Crossword.class).putExtra("id", v.getTag().toString()).putExtra("textColor", textColor));
                        }
                    });
                }
            });


        }

        private void showInterstitial() {
            sp.edit().putInt("admob", !sp.contains("admob") ? 1 : sp.getInt("admob", 0) + 1).commit();
            if (adMobInterstitial.isLoaded()) {
//                    if (sp.getInt("admob", 0) >= adMobInterstitialInterval) {
//                        sp.edit().putInt("admob", 0).commit();
                adMobInterstitial.show(); // show
//                    }
            } else if (!adMobInterstitial.isLoading() && ((ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE)).getActiveNetworkInfo() != null)
                adMobInterstitial.loadAd(adRequest); // load
//            finish();
        }

        @Override
        public int getCount() {
            return num_pages;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view.equals(object);
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            ((ViewPager) container).removeView((View) object);
        }
    }

    private void showLock() {
        CustomDialogClass cdd2 = new CustomDialogClass(this, LOCK_DIALOG, LOCK_DIALOG, color, this);
        cdd2.show();
    }

    @Override
    protected void onDestroy() {
        // destroy AdMob

        if (adMobInterstitial != null) {
            adMobInterstitial.setAdListener(null);
            adMobInterstitial = null;
        }
        adRequest = null;

        super.onDestroy();
    }

    private void adMobInter() {
        // make AdMob request
        Builder builder = new Builder();
//            if (getResources().getBoolean(R.bool.admob_test))
//                builder.addTestDevice(AdRequest.DEVICE_ID_EMULATOR).addTestDevice(
//                        MD5(Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID)));
        adRequest = builder.build();

        // AdMob Interstitial
        adMobInterstitial = new InterstitialAd(this);
        adMobInterstitial.setAdUnitId(getString(R.string.adMob_interstitial));
        adMobInterstitial.setAdListener(new AdListener() {
            public void onAdClosed() {
                if (((ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE)).getActiveNetworkInfo() != null)
                    adMobInterstitial.loadAd(adRequest);
            }
        });

        if (((ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE)).getActiveNetworkInfo() != null) {
            // AdMob Banner

            adMobInterstitial.loadAd(adRequest);
        }
    }


}