package com.olden.crossword;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatDelegate;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class CustomDialogClass extends Dialog implements
        android.view.View.OnClickListener {

    public Activity c;
    public Dialog d;
    private LinearLayout successContainer, hintContainer, lockContainer;
    private RelativeLayout headerHint, headerSuccess, headerLock;
    private TextView tvScoreDialog, tvLevelDialog, tvNonton, tvNontonhint,
            tvCancel, tvHint, tvMenang, tvNilai, tvBagikan, tvLanjut;
    public String scoreDialog, levelDialog;
    private LinearLayout shareDialog, backDialog, nextDialog, hintDialog, cancelDialog;
    public static final String HINT_DIALOG = "hint_dialog_value";
    public static final String NEXT_DIALOG = "NEXT_DIALOG";
    public static final String SHARE_DIALOG = "SHARE_DIALOG ";
    public static final String LOCK_DIALOG = "LOCK_DIALOG";
    private int color;

    private CallbackThis callbackThis;

    public CustomDialogClass(Activity a, String method, String level, int color, CallbackThis callbackThis) {
        super(a);
        // TODO Auto-generated constructor stub

        this.c = a;
        this.scoreDialog = method;
        this.callbackThis = callbackThis;
        this.levelDialog = level;
        this.color = color;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_custom);
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        hintContainer = findViewById(R.id.container_hint_dialog);
        successContainer = findViewById(R.id.container_success_dialog);
        lockContainer= findViewById(R.id.container_lock_dialog);

        if (scoreDialog.equals(HINT_DIALOG)) {
            initHintView();
        } else if (scoreDialog.equals(LOCK_DIALOG)) {
            initLockDialog();
        } else
        {
            initSuccessView();
        }
//        backDialog.setOnClickListener(this);

    }

    private void initLockDialog() {
        lockContainer.setVisibility(View.VISIBLE);
        successContainer.setVisibility(View.GONE);
        hintContainer.setVisibility(View.GONE);
        headerLock = findViewById(R.id.color_header_lock);
        headerLock.setBackgroundColor(color);
    }

    private void initHintView() {
        hintContainer.setVisibility(View.VISIBLE);
        successContainer.setVisibility(View.GONE);
        lockContainer.setVisibility(View.GONE);
        hintDialog = findViewById(R.id.btn_hint_dialog);
        cancelDialog = findViewById(R.id.btn_cancel_dialog);
        headerHint = findViewById(R.id.color_header);
        tvCancel = findViewById(R.id.tv_cancel_dialog);
        tvHint = findViewById(R.id.tv_hint_hint);
        tvNonton = findViewById(R.id.nonton_video);
        tvNontonhint = findViewById(R.id.nonton_video_hint);

        headerHint.setBackgroundColor(color);

        hintDialog.setOnClickListener(this);
        cancelDialog.setOnClickListener(this);

    }

    private void initSuccessView() {
        successContainer.setVisibility(View.VISIBLE);
        hintContainer.setVisibility(View.GONE);
        lockContainer.setVisibility(View.GONE);
        shareDialog = findViewById(R.id.btn_share_dialog);
        nextDialog = findViewById(R.id.btn_next_dialog);
        backDialog = findViewById(R.id.btn_back_dialog);
        tvScoreDialog = findViewById(R.id.tv_score_dialog);
        tvLevelDialog = findViewById(R.id.tv_level_dialog);
        headerSuccess = findViewById(R.id.color_header_success);
        tvMenang = findViewById(R.id.menang);
        tvNilai = findViewById(R.id.nilai);
        tvBagikan = findViewById(R.id.bagikan);
        tvLanjut = findViewById(R.id.lanjut);


        headerSuccess.setBackgroundColor(color);

        tvScoreDialog.setText(scoreDialog);
        tvLevelDialog.setText(c.getResources().getString(R.string.messages_text) + " " + levelDialog);
        shareDialog.setOnClickListener(this);
        nextDialog.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_share_dialog:
                callbackThis.whatCallback(SHARE_DIALOG);
                break;
            case R.id.btn_next_dialog:
                callbackThis.whatCallback(NEXT_DIALOG);
                break;
            case R.id.btn_hint_dialog:
                callbackThis.whatCallback(HINT_DIALOG);
                break;
            case R.id.btn_cancel_dialog:
                this.dismiss();
                break;
        }
        dismiss();
    }
}
