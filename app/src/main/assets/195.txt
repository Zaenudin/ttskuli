{
  "cols": 13,
  "rows": 13,
  "cells": [
    "B",
    "O",
    "S",
    "T",
    "E",
    "R",
    "B",
    "R",
    "A",
    "N",
    "D",
    "",
    "",
    "O",
    "",
    "A",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "U",
    "",
    "S",
    "",
    "T",
    "A",
    "D",
    "B",
    "I",
    "R",
    "",
    "I",
    "",
    "S",
    "",
    "",
    "",
    "P",
    "",
    "",
    "",
    "",
    "",
    "I",
    "N",
    "F",
    "A",
    "K",
    "",
    "H",
    "A",
    "P",
    "U",
    "S",
    "",
    "",
    "",
    "D",
    "",
    "P",
    "",
    "",
    "",
    "M",
    "",
    "L",
    "",
    "",
    "",
    "",
    "I",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "T",
    "I",
    "P",
    "S",
    "",
    "G",
    "R",
    "A",
    "P",
    "M",
    "A",
    "G",
    "E",
    "R",
    "",
    "",
    "M",
    "",
    "O",
    "",
    "",
    "U",
    "A",
    "",
    "E",
    "",
    "A",
    "",
    "",
    "E",
    "",
    "",
    "",
    "",
    "Y",
    "L",
    "A",
    "N",
    "",
    "F",
    "R",
    "E",
    "S",
    "H",
    "C",
    "A",
    "R",
    "E",
    "I",
    "",
    "T",
    "",
    "L",
    "",
    "",
    "",
    "",
    "",
    "R",
    "",
    "N",
    "N",
    "",
    "A",
    "",
    "U",
    "",
    "",
    "",
    "",
    "",
    "G",
    "",
    "G",
    "G",
    "U",
    "R",
    "U",
    "",
    "",
    "",
    "B",
    "E",
    "T",
    "O",
    "N",
    ""
  ],
  "words": [
    "0,1,2,3,4,5,6,7,8,9,10",
    "0,13,26",
    "2,15,28,41,54,67",
    "53,54,55,56,57",
    "56,69,82,95,108,121,134,147",
    "91,92,93,94,95",
    "91,104,117,130,143,156",
    "93,106,119,132,145,158",
    "117,118,119",
    "156,157,158,159",
    "82,83,84,85",
    "85,98,111,124",
    "121,122,123,124,125,126,127,128,129",
    "127,140,153,166",
    "90,103,116,129,142,155",
    "87,88,89,90",
    "35,48,61,74,87,100",
    "47,48,49,50,51",
    "24,37,50,63",
    "163,164,165,166,167"
  ],
  "questions": [
   "OBAT IKAN",
   "ATASAN KARYAWAN",
   "SATUAN PENGAMANAN",
   "MENGHILANGKAN SESUATU YANG ADA MENJADI TAK ADA",
   "DENGAN ALAT TERTENTU",
   "OBAT BERSIN DAN PILEK",
   "SUDAH PEWE DITEMPAT",
   "MENGAMBIL BARANG ORANG LAIN",
   "MAJU TAK …(BERANI TANPA RAGU)",
   "LOCAL AREA NETWORK",
   "ORANG YANG PEKERJAANNYA MENGAJAR",
   "CARA MELAKUKAN SESUATU DENGAN AKAL",
   "PUKULAN KERAS POLY",
   "NAMA MINYAK",
   "MESIN PENUNJUK PEMBAYARAN PADA TRANSPORTASI",
   "MEMUSINGKAN",
   "ROMBONGAN",
   "TUMBUHAN TROPIS MENGHASILKAN ZAT CELUP BIRU",
   "PEMBERIAN HARTA",
   "MENGHAPUS KERINGAT",
   "CAMPURAN SEMEN DAN KRIKIL"

  ]
}